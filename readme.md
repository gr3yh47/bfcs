# README #

### Bright Flame Tactics ###

* Created in 2012 for the open-source game competition [Liberated Pixel Cup](http://lpc.opengameart.org/)
* Engine was loosely based on the [QQ Pygame Example](http://pygame.org/project-QQ-1889-.html)
* Turn-based tactical strategy game heavily inspired by the games Fire Emblem and Shining Force
* All source is licensed under GPL v3 - see COPYING.txt for details
* originally called 'Bright Flame Crest Squad' until i decided the name was too cumbersome

### How do I get set up? ###

* python 2.7.x
* pygame 1.9.x - http://www.pygame.org/download.shtml
* someone to play with! this is a 2-player hotseat multiplayer game. 
* run python game.py
* **Hit 'H' to show/hide help in menus and ingame**