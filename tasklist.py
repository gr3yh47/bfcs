Tasks:

    http://en.wikipedia.org/wiki/Five_elements_%28Japanese_philosophy%29
    for spell icons. water and fire are the same as they already are
    air for tornado, void for lightning

    High Level:
        #Implement Menu control system for arrows + 2 buttons
        create a menu-based control system
        implement mouse control and integrate with menu system
        further reduce memory footprint
        add network multiplayer
        add AI
        more maps!
        controller support
        level object
        battle preview of some sort
        #'flash' the character who is up next {Standing animation implemented}
        indicate which players turn it is (p1 or p2)
        Standing animation for active character!
        ctf flag object, which has to be returned to the location, can be dropped, uncarried flag can be returned at the cost of a turn
        ice freeze chance, 1 turn
        go through the below list and reactivate items that were canned due to time constraints
    
    GUI:
        #%%finish the char seleciton screen including sprites. 
        #SCRATCHED update stats panel implementation - might ditch this
        implement collection.mini_img for battle display
        #%design, code start screen
        #map selection screen
        #fix horse image
        
        #code status view where players can check the status of given units
        #make a status help panel
        #finish character creation and tie it into the game test
        #implement help dialogue
        #determine help dialogue implementation (h to display/hide help)
        #FIX SPRITE BLEED ON ROSTER SCREEN (mage and archer/slots [3] and [4] only)
    
    items:
        #%list of items and their effects.
        #%code items above
        #%finish making sprite outfits
        
        #code weapon randomization for team members 
        #hard code player outfits
    
    Battle:
        death animation
        #%%fix fight animation pt 3 (ranged vs direct)
        #fix fight animation pt 4 (numbers position)
        #SCRATCHED implement heal
        #fix arrow image
        #%formulae
        #%%mage dodge image animation - going to have to deal with animation length of attack
        #%%code sizes for weapons or convert all to 320
        
        #code a different animation for ranged attacks
        #figure out how heal will work 
        #code weapon drawing
        #make a dodge image for battle message
        #fix bow image
        #implement battle messages system for crit and dodge
        #make guys die
        #fix fight animation pt 1 (cleanup update() vs next())
        #fix fight animation pt 2 (positioning and distance)
        # ditched counters... figure out fight flow (counters? might ditch counters) and 
        

    General:    
        #code level up display
        #implement stat changes for level up
        #%code capture victory condition
        create and implement a data driven level file standard
        event object pursuant to above
        %make maps for levels
        
        #code names
        #make a map in tiled
        #implement pytmx
        #implement overlays
        #get highlights working with pytmx
        #implement start positions with tmx maps
        #code a defeat all enemies victory condition

    AI:
        code a basic closest enemy AI
        create a prioritized list of ai strategies
        implement above list
    
    Sound:
        
        #randomize hurt sounds/attack sounds
        #Implement inventory sound
        #Sound data structure  - Dict of Dicts (...of lists for randomizable sounds)
    
    Low Priority:
        
        support system
        menu system??
        alter weapon system to allow for more weapons
        finish implementing equipment
        determine naming scheme for clothing items
        implement outfit selection/character customization
        
        #if a friendly is at the edge of movement, attack highlights as though you could attack from that square