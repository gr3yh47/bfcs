you need:

python 2.7.2
http://www.python.org/download/releases/2.7.2/

pygame 1.9.x
http://www.pygame.org/download.shtml

someone to play with! this is a 2-player hotseat multiplayer game. 
I recommend using 2 keyboards.

unzip this and navigate to the directory
run:
python game.py

enjoy!