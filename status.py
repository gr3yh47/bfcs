''' 
    Copyright (c) 2012, Steve Peck aka gr3yh47 <gr3yh47@gmail.com>
    See COPYING.txt for full license text


    status.py
    This file is part of Bright Flame Tactics.

    Bright Flame Tactics is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bright Flame Tactics is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Bright Flame Tactics.  If not, see <http://www.gnu.org/licenses/>.
    
    some code derived from qq, Copyright (c) 2008, 2009 Radomir Dopieralski <qq@sheep.art.pl>. see qq.rar for source and license
'''
import pygame
import os
import weakref
from map import TileCache

col1 = ['lvl', 'str', 'skl', 'int']
col2 = ['mvt', 'dfn', 'spd', 'lck']
cola = ['hp', 'xp']
colb = ['max', 'nxt']

# class StatPanel(object):
    # '''displays pages of player stats aka Collection()s'''
    # def __init__(self, player_list):
        # self.pages = []
        #self.background = pygame.image.load('.\\graphics\\statbg.png').convert_alpha
        # collection_list = []
        # for player in player_list:
            # if len(collection_list) == 4:
                # self.pages.append(collection_list)
                # collection_list = []
            # collection_list.append(player.collection)
        # if collection_list:
            # self.pages.append(collection_list)
        
            
    # def update(self, page = 0):
        # pos_y = 0
        # image = pygame.Surface((100, 256))
        # for collection in self.pages[page]:
            # collection.refresh()
            # image.blit(collection.image, (2, pos_y))
            # pos_y += 120
        
        # return image        
 
    
 
#collection of stat display elements for one player 
class Collection(object):
    '''representation of one player's stats to be displayed on the StatPanel()'''
    
    def __init__(self, player):
        self.player = weakref.proxy(player)
        self.stats = player.stats
        self.hp_bar = Bar(self.stats['hp'], self.stats['max'])
        self.xpdenom = player.xpstep
        self.xpnumer = self.stats['xp'] - (self.stats['nxt'] - self.xpdenom)
        self.xp_bar = Bar(self.xpnumer, self.xpdenom, 'xp', 1)
        self.pos = (0, 0)
        self.image = pygame.Surface((100, 134), 1)
        self.image = self.image.convert_alpha()
        self.image.fill((255, 255, 255, 0))
        self.background = pygame.image.load(os.path.join('.', 'graphics', 'statbg.png')).convert_alpha()
        self.refresh()
   
    def level_up(self):
        log_path = os.path.join('.', 'logs', '')
        try:
            g = open(log_path + self.player.name + '-collection_level_up.log', 'a')
        except IOError:
            g = open(log_path + self.player.name + '-collection_level_up.log', 'w')
        g.write('entering collection level up\n')
        
        
        '''called from Character.level_up, readjusts xp_bar and hp_bar, then calls self.refresh()'''
        self.stats = self.player.stats
        g.write('collection stats are:\n')
        for stat, num in self.stats.items():
            g.write(stat + ': ' + str(num) + '\n')
        self.xpdenom = self.player.xpstep
        self.xpnumer = self.stats['xp'] - (self.stats['nxt'] - self.xpdenom)
        self.xp_bar = Bar(self.xpnumer, self.xpdenom, 'xp', 1)
        self.hp_bar = Bar(self.stats['hp'], self.stats['max'])
        g.write('xp bar = ' + str(self.xpnumer) + '/' + str(self.xpdenom) + '\n')
        g.write('hp bar = ' + str(self.stats['hp']) + '/' + str(self.stats['max']) + '\n')
        self.refresh()
        g.write('\n\n\n')
    
    def refresh(self):
        '''refreshes all stats in the Collection()'''
        font_file = os.path.join('.', 'fonts', 'UbuntuMono-R.ttf')
        self.image.blit(self.background, (0, 0))
        self.stats = self.player.stats
        self.hp_bar.update(self.stats['hp'])
        self.xpnumer = self.stats['xp'] - (self.stats['nxt'] - self.xpdenom)
        self.xp_bar.update(self.xpnumer)
        this_font = pygame.font.Font(font_file, 16)
        color = (0, 0, 0)
        
        name_text = this_font.render(self.player.name, 1, color)
        this_font = pygame.font.Font(font_file, 12)
        cls_text = this_font.render(self.player.cls, 1, color)
        hp_text = this_font.render('hp: ' + str(int(self.stats['hp'])) + '/' + str(self.stats['max']), 1, color)
        xp_text = this_font.render('xp: ' + str(self.xp_bar.numer) + '/' + str(self.xp_bar.denom), 1, color)
        name_x = (100 - name_text.get_width()) / 2
        pos_y = 4
        self.image.blit(name_text, (name_x, pos_y))
        pos_y += 16
        cls_x = (100 - cls_text.get_width()) / 2
        self.image.blit(cls_text, (cls_x, pos_y))
        pos_y += 12
        self.image.blit(hp_text, (4, pos_y))
        pos_y += 12
        self.image.blit(self.hp_bar.image, (4, pos_y))
        pos_y += 12
        self.image.blit(xp_text, (4, pos_y))
        pos_y += 12
        self.image.blit(self.xp_bar.image, (4, pos_y))
        #self.stat_imgs = []
        color = (0, 0, 0)
        text_y = pos_y + 12
        for s1, s2 in zip(col1, col2):
            img1 = this_font.render(s1 + ': ' + str(self.stats[s1]), 1, color)
            img2 = this_font.render(s2 + ': ' + str(self.stats[s2]), 1, color)
            self.image.blit(img1, (4, text_y))
            self.image.blit(img2, (self.image.get_width() - 42, text_y))
            text_y += 12
            #this_string = this_font.render(k + ': ' + v, 1, (255, 0, 0)
        #half = len(this_string)/2
        #for t1, t2 in zip(this_string[:half], this_string[half:]):
            
        
       
 
class Bar(object):
    '''object for Collection().hp_bar and xp_bar'''
    def __init__(self, numer = 10, denom = 10, stat = 'hp', color = 0):
        self.frames = TileCache('bars6.png', 90, 60).cache
        self.color = color
        self.image = self.frames[self.color][29]
        self.numer = numer #numerator, or current stat position
        self.denom = denom #denominator, or max stat position
        self.steps = 30 #hardcoded here, only has to be changed here
        self.stat = stat
        
        
    def update(self, numer, denom = None):
        if denom:
            self.denom = denom
        self.numer = numer
        index = max((int((float(self.numer) / self.denom) * 30) - 1), 0)
        index = min(index, 29)
        self.image = self.frames[self.color][index]
        
        


